# coding: utf-8
lib = File.expand_path('../lib', __FILE__)
$LOAD_PATH.unshift(lib) unless $LOAD_PATH.include?(lib)
require 'parcel/version'

Gem::Specification.new do |spec|
  spec.name          = "webhook-parcel"
  spec.version       = Parcel::VERSION
  spec.authors       = ["DevLocker Engineering"]
  spec.email         = ["dev@devlocker.io"]
  spec.summary       = %q{Ruby wrapper for Github Webhook Payloads}
  spec.description   = %q{Provide objects for Github Webhook Payloads as well as parse URI's, Filenames into Ruby objects}
  spec.homepage      = "https://github.com/devlocker/parcel"
  spec.license       = "MIT"

  spec.files         = `git ls-files -z`.split("\x0")
  spec.executables   = spec.files.grep(%r{^bin/}) { |f| File.basename(f) }
  spec.test_files    = spec.files.grep(%r{^(test|spec|features)/})
  spec.require_paths = ["lib"]

  spec.add_dependency "virtus", "~> 1.0"
  spec.add_development_dependency "bundler", "~> 1.7"
  spec.add_development_dependency "rake"
  spec.add_development_dependency "minitest"
end
