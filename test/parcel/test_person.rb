require "helper"

class TestPerson < MiniTest::Test
  def setup
    json = JSON.parse(File.read("test/commit_webhook.json"))
    @person = Parcel::Person.new(json["sender"])
  end

  def test_login
    assert_equal "mikeastock", @person.login
  end

  def test_id
    assert_equal 1907044, @person.id
  end

  def test_avatar_url
    assert_equal "https://avatars.githubusercontent.com/u/1907044?v=3", @person.avatar_url
  end

  def test_gravatar_id
    assert_equal "", @person.gravatar_id
  end

  def test_url
    assert_equal "https://api.github.com/users/mikeastock", @person.url
  end

  def test_html_url
    assert_equal "https://github.com/mikeastock", @person.html_url
  end

  def test_followers_url
    assert_equal "https://api.github.com/users/mikeastock/followers", @person.followers_url
  end

  def test_following_url
    assert_equal "https://api.github.com/users/mikeastock/following{/other_user}", @person.following_url
  end

  def test_gists_url
    assert_equal "https://api.github.com/users/mikeastock/gists{/gist_id}", @person.gists_url
  end

  def test_starred_url
    assert_equal "https://api.github.com/users/mikeastock/starred{/owner}{/repo}", @person.starred_url
  end

  def test_subscriptions_url
    assert_equal "https://api.github.com/users/mikeastock/subscriptions", @person.subscriptions_url
  end

  def test_organizations_url
    assert_equal "https://api.github.com/users/mikeastock/orgs", @person.organizations_url
  end

  def test_repos_url
    assert_equal "https://api.github.com/users/mikeastock/repos", @person.repos_url
  end

  def test_events_url
    assert_equal "https://api.github.com/users/mikeastock/events{/privacy}", @person.events_url
  end

  def test_received_events_url
    assert_equal "https://api.github.com/users/mikeastock/received_events", @person.received_events_url
  end

  def test_type
    assert_equal "User", @person.type
  end

  def test_site_admin
    assert_equal false, @person.site_admin
  end
end
