require "helper"

class TestCommit < MiniTest::Test
  def setup
    json = JSON.parse(File.read("test/commit_webhook.json"))
    @commit = Parcel::Commit.new(json["commits"].first)
  end

  def test_id
    assert_equal "59f4f57da2e4eff5c349141bc9e267d47f70c417", @commit.id
  end

  def test_distinct
    assert_equal true, @commit.distinct
  end

  def test_message
    assert_equal "Add headers to webhook request", @commit.message
  end

  def test_timestamp
    assert_equal DateTime.parse("2014-11-30T20:34:52-08:00"), @commit.timestamp
  end

  def test_url
    assert_equal "https://github.com/mikeastock/devlocker/commit/59f4f57da2e4eff5c349141bc9e267d47f70c417", @commit.url
  end

  def test_author
    assert_instance_of Parcel::Person, @commit.author
  end

  def test_committer
    assert_instance_of Parcel::Person, @commit.committer
  end

  def test_added
    assert_equal [], @commit.added
  end

  def test_removed
    assert_equal [], @commit.removed
  end

  def test_modified
    assert_equal ["spec/controllers/github/repos_controller_spec.rb"], @commit.modified
  end
end
