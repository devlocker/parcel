require "helper"

class TestRepo < MiniTest::Test
  def setup
    json = JSON.parse(File.read("test/commit_webhook.json"))
    @repo = Parcel::Repository.new(json["repository"])
  end

  def test_id
    assert_equal 27007704, @repo.id
  end

  def test_name
    assert_equal "devlocker", @repo.name
  end


  def test_full_name
    assert_equal "mikeastock/devlocker", @repo.full_name
  end

  def test_owner
    assert_equal "mikeastock", @repo.owner[:name]
    assert_equal "mikeastock@gmail.com", @repo.owner[:email]
  end

  def test_private
    assert_equal true, @repo.private
  end

  def test_html_url
    assert_equal "https://github.com/mikeastock/devlocker", @repo.html_url
  end

  def test_description
    assert_equal "", @repo.description
  end

  def test_fork
    assert_equal true, @repo.fork
  end

  def test_url
    assert_equal "https://github.com/mikeastock/devlocker", @repo.url
  end

  def test_forks_url
    assert_equal "https://api.github.com/repos/mikeastock/devlocker/forks", @repo.forks_url
  end

  def test_keys_url
    assert_equal "https://api.github.com/repos/mikeastock/devlocker/keys{/key_id}", @repo.keys_url
  end

  def test_collaborators_url
    assert_equal "https://api.github.com/repos/mikeastock/devlocker/collaborators{/collaborator}", @repo.collaborators_url
  end

  def test_teams_url
    assert_equal "https://api.github.com/repos/mikeastock/devlocker/teams", @repo.teams_url
  end

  def test_hooks_url
    assert_equal "https://api.github.com/repos/mikeastock/devlocker/hooks", @repo.hooks_url
  end

  def test_issue_events_url
    assert_equal "https://api.github.com/repos/mikeastock/devlocker/issues/events{/number}", @repo.issue_events_url
  end

  def test_events_url
    assert_equal "https://api.github.com/repos/mikeastock/devlocker/events", @repo.events_url
  end

  def test_assignees_url
    assert_equal "https://api.github.com/repos/mikeastock/devlocker/assignees{/user}", @repo.assignees_url
  end

  def test_branches_url
    assert_equal "https://api.github.com/repos/mikeastock/devlocker/branches{/branch}", @repo.branches_url
  end

  def test_tags_url
    assert_equal "https://api.github.com/repos/mikeastock/devlocker/tags", @repo.tags_url
  end

  def test_blobs_url
    assert_equal "https://api.github.com/repos/mikeastock/devlocker/git/blobs{/sha}", @repo.blobs_url
  end

  def test_git_tags_url
    assert_equal "https://api.github.com/repos/mikeastock/devlocker/git/tags{/sha}", @repo.git_tags_url
  end

  def test_git_refs_url
    assert_equal "https://api.github.com/repos/mikeastock/devlocker/git/refs{/sha}", @repo.git_refs_url
  end

  def test_trees_url
    assert_equal "https://api.github.com/repos/mikeastock/devlocker/git/trees{/sha}", @repo.trees_url
  end

  def test_statuses_url
    assert_equal "https://api.github.com/repos/mikeastock/devlocker/statuses/{sha}", @repo.statuses_url
  end

  def test_languages_url
    assert_equal "https://api.github.com/repos/mikeastock/devlocker/languages", @repo.languages_url
  end

  def test_stargazers_url
    assert_equal "https://api.github.com/repos/mikeastock/devlocker/stargazers", @repo.stargazers_url
  end

  def test_contributors_url
    assert_equal "https://api.github.com/repos/mikeastock/devlocker/contributors", @repo.contributors_url
  end

  def test_subscribers_url
    assert_equal "https://api.github.com/repos/mikeastock/devlocker/subscribers", @repo.subscribers_url
  end

  def test_subscription_url
    assert_equal "https://api.github.com/repos/mikeastock/devlocker/subscription", @repo.subscription_url
  end

  def test_commits_url
    assert_equal "https://api.github.com/repos/mikeastock/devlocker/commits{/sha}", @repo.commits_url
  end

  def test_git_commits_url
    assert_equal "https://api.github.com/repos/mikeastock/devlocker/git/commits{/sha}", @repo.git_commits_url
  end

  def test_comments_url
    assert_equal "https://api.github.com/repos/mikeastock/devlocker/comments{/number}", @repo.comments_url
  end

  def test_issue_comment_url
    assert_equal "https://api.github.com/repos/mikeastock/devlocker/issues/comments/{number}", @repo.issue_comment_url
  end

  def test_contents_url
    assert_equal "https://api.github.com/repos/mikeastock/devlocker/contents/{+path}", @repo.contents_url
  end

  def test_compare_url
    assert_equal "https://api.github.com/repos/mikeastock/devlocker/compare/{base}...{head}", @repo.compare_url
  end

  def test_merges_url
    assert_equal "https://api.github.com/repos/mikeastock/devlocker/merges", @repo.merges_url
  end

  def test_archive_url
    assert_equal "https://api.github.com/repos/mikeastock/devlocker/{archive_format}{/ref}", @repo.archive_url
  end

  def test_downloads_url
    assert_equal "https://api.github.com/repos/mikeastock/devlocker/downloads", @repo.downloads_url
  end

  def test_issues_url
    assert_equal "https://api.github.com/repos/mikeastock/devlocker/issues{/number}", @repo.issues_url
  end

  def test_pulls_url
    assert_equal "https://api.github.com/repos/mikeastock/devlocker/pulls{/number}", @repo.pulls_url
  end

  def test_milestones_url
    assert_equal "https://api.github.com/repos/mikeastock/devlocker/milestones{/number}", @repo.milestones_url
  end

  def test_notifications_url
    assert_equal "https://api.github.com/repos/mikeastock/devlocker/notifications{?since,all,participating}", @repo.notifications_url
  end

  def test_labels_url
    assert_equal "https://api.github.com/repos/mikeastock/devlocker/labels{/name}", @repo.labels_url
  end

  def test_created_at
    assert_equal 1416680049, @repo.created_at
  end

  def test_updated_at
    assert_equal "2014-11-25T04:30:40Z", @repo.updated_at
  end

  def test_pushed_at
    assert_equal 1417408510, @repo.pushed_at
  end

  def test_git_url
    assert_equal "git://github.com/mikeastock/devlocker.git", @repo.git_url
  end

  def test_ssh_url
    assert_equal "https://github.com/mikeastock/devlocker.git", @repo.clone_url
  end

  def test_clone_url
    assert_equal "https://github.com/mikeastock/devlocker.git", @repo.clone_url
  end

  def test_svn_url
    assert_equal "https://github.com/mikeastock/devlocker", @repo.svn_url
  end

  def test_homepage
    assert_equal nil, @repo.homepage
  end

  def test_size
    assert_equal 358, @repo.size
  end

  def test_stargazers_count
    assert_equal 0, @repo.stargazers_count
  end

  def test_watchers_count
    assert_equal 0, @repo.watchers_count
  end

  def test_language
    assert_equal "Ruby", @repo.language
  end

  def test_has_issues
    assert_equal false, @repo.has_issues
  end

  def test_has_downloads
    assert_equal true, @repo.has_downloads
  end

  def test_has_wiki
    assert_equal true, @repo.has_wiki
  end

  def test_has_pages
    assert_equal false, @repo.has_pages
  end

  def test_forks_count
    assert_equal 0, @repo.forks_count
  end

  def test_mirror_url
    assert_equal nil, @repo.mirror_url
  end

  def test_open_issues_count
    assert_equal 2, @repo.open_issues_count
  end

  def test_forks
    assert_equal 0, @repo.forks
  end

  def test_open_issues
    assert_equal 2, @repo.open_issues
  end

  def test_watchers
    assert_equal 0, @repo.watchers
  end

  def test_default_branch
    assert_equal "master", @repo.default_branch
  end

  def test_stargazers
    assert_equal 0, @repo.stargazers
  end

  def test_master_branch
    assert_equal "master", @repo.master_branch
  end
end
