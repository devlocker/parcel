require "helper"

class TestPayload < MiniTest::Test
  def setup
    json = JSON.parse(File.read("test/commit_webhook.json"))
    @payload = Parcel::Payload.new(json)
  end

  def test_ref
    assert_equal "refs/heads/mas/webhooks", @payload.ref
  end

  def test_before
    assert_equal "905ad37282d0cee53139f4ec8396a7d6985dd2de", @payload.before
  end

  def test_after
    assert_equal "59f4f57da2e4eff5c349141bc9e267d47f70c417", @payload.after
  end

  def test_created
    assert_equal false, @payload.created
  end

  def test_deleted
    assert_equal false, @payload.deleted
  end

  def test_forced
    assert_equal false, @payload.forced
  end

  def test_base_ref
    assert_equal nil, @payload.base_ref
  end

  def test_compare
    assert_equal "https://github.com/mikeastock/devlocker/compare/905ad37282d0...59f4f57da2e4", @payload.compare
  end

  def test_commits
    assert_instance_of Parcel::Commit, @payload.commits[0]
  end

  def test_head_commit
    assert_instance_of Parcel::Commit, @payload.head_commit
  end

  def test_repository
    assert_instance_of Parcel::Repository, @payload.repository
  end

  def test_pusher
    assert_instance_of Parcel::Person, @payload.pusher
  end

  def test_sender
    assert_instance_of Parcel::Person, @payload.sender
  end
end
