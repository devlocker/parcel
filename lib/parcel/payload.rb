class Parcel::Payload
  include Virtus.model

  attribute :ref,         String
  attribute :before,      String
  attribute :after,       String
  attribute :created,     Boolean
  attribute :deleted,     Boolean
  attribute :forced,      Boolean
  attribute :base_ref,    String
  attribute :compare,     String
  attribute :commits,     Array[Parcel::Commit]
  attribute :head_commit, Parcel::Commit
  attribute :repository,  Parcel::Repository
  attribute :pusher,      Parcel::Person
  attribute :sender,      Parcel::Person
end
