class Parcel::Commit
  include Virtus.model

  attribute :id,        Integer
  attribute :distinct,  Boolean
  attribute :message,   String
  attribute :timestamp, DateTime
  attribute :url,       String
  attribute :author,    Parcel::Person
  attribute :committer, Parcel::Person
  attribute :added,     Array[String]
  attribute :removed,   Array[String]
  attribute :modified,  Array[String]
end
