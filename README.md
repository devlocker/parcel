## Parcel
Ruby wrapper for Github Webhook Payloads

[![Build Status](https://travis-ci.org/devlocker/parcel.svg?branch=master)](https://travis-ci.org/devlocker/parcel)

## Installation

Add this line to your application's Gemfile:

```ruby
gem "parcel"
```

And then execute:

    $ bundle

Or install it yourself as:

    $ gem install parcel

## Usage

To use, create a Parcel::Payload

```ruby
# hash of webhook JSON
webhook_hash
# => {"ref"=>"refs/heads/mas/webhooks",
#  "before"=>"905ad37282d0cee53139f4ec8396a7d6985dd2de",
#  "after"=>"59f4f57da2e4eff5c349141bc9e267d47f70c417",
#  "created"=>false,
#  "deleted"=>false,
#  "forced"=>false,
#  "base_ref"=>nil,
#  "compare"=>"https://github.com/mikeastock/devlocker/compare/905ad37282d0...59f4f57da2e4",
#  "commits"=>
#   [{"id"=>"59f4f57da2e4eff5c349141bc9e267d47f70c417",
#     "distinct"=>true,
#     "message"=>"Add headers to webhook request",
#     "timestamp"=>"2014-11-30T20:34:52-08:00",

parcel = Parcel::Payload.new(webhook_hash)
# => #<Parcel::Payload:0x007ffb552ae6c8
#  @after="59f4f57da2e4eff5c349141bc9e267d47f70c417",
#  @base_ref=nil,
#  @before="905ad37282d0cee53139f4ec8396a7d6985dd2de",
#  @commits=
#   [#<Parcel::Commit:0x007ffb552964d8
#     @added=[],
#     @author=
#      #<Parcel::Person:0x007ffb5525dd18
#       @avatar_url=nil,
#       @email="mikeastock@gmail.com",
#       @events_url=nil,
#       @followers_url=nil,
#       @following_url=nil,
#       @gists_url=nil,
#       @gravatar_id=nil,
#       @html_url=nil,
#       @id=nil,
#       @login=nil,
#       @name="Michael Stock",
```

## Contributing

1. Fork it ( https://github.com/devlocker/parcel/fork )
2. Create your feature branch (`git checkout -b my-new-feature`)
3. Commit your changes (`git commit -am 'Add some feature'`)
4. Push to the branch (`git push origin my-new-feature`)
5. Create a new Pull Request
